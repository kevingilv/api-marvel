const express = require('express');
const app = express();
const PostUser = require('./routes/user.route');
const Cart = require('./routes/cart.route');
const jwt = require('jsonwebtoken');
const constants = require('./constants');
const cors = require('cors');

//Setting port
app.set('port', process.env.POST || 9000);
//Middleware
app.use(express.json());
//Configure headers and cors
app.use(cors());

app.use(function (req, res, next) {
    let isAuthPage = req.url.indexOf('/login');
    let isNewUser = req.url.indexOf('/create');
    if (isAuthPage < 0 && isNewUser < 0) {
        const berearHeader = req.headers['authorization'];
        if (typeof berearHeader !== 'undefined') {
            const berear = berearHeader.split(" ");
            const berearToken = berear[1];
            req.token = berearToken;
            jwt.verify(req.token, constants.SECRET_KEY, (err, decoded) => {
                if (err) {
                    res.sendStatus(403);
                }
                req.token = decoded;
                next();
            });
        } else {
            res.sendStatus(401);
        }
    } else {
        next();
    }
});

app.use('/user', PostUser);
app.use('/cart', Cart);

app.listen(app.get('port'), () => {
    console.log('Starting service ....');
});