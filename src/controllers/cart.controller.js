const controller = {}
const CartApplication = require('../applications/cart.app');

controller.create = async (req,res) => {
    let data = [];
    try {
        data = await CartApplication.create(req, res);
    } catch (error) {
        return res.status(500).json({ success: false, data: [] });
    }
    res.status(200).json({ success: true, data: data });
}

controller.getAllByUserId = async (req,res) => {
    let data = [];
    try {
        data = await CartApplication.getAllByUserId(req, res);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ success: false, data: [] });
    }
    res.status(200).json({ success: true, data: data });
}


module.exports = controller;
