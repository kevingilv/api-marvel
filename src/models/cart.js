const Sequelize = require('sequelize');
let database = require('../database/database');
let user = require('./user');

let nametable = 'cart';

let cart = database.define(nametable, {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    characterId: { type: Sequelize.INTEGER, allowNull: false },
    image: { type: Sequelize.STRING, allowNull: false },
    characterName: { type: Sequelize.STRING, allowNull: false },
    quantity: { type: Sequelize.INTEGER, allowNull: false },
    //foreign key
    userId: {
        type: Sequelize.INTEGER,
        references: {
            model: user,
            key: 'id'
        }
    }
})

cart.belongsTo(user);

module.exports = cart;
