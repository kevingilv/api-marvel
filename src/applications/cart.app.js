let sequelize = require('../database/database');
let cart = require('../models/cart');
let user = require('../models/user');

sequelize.sync();

async function create(req, res) {

    let data = [];
    let creatingUser = [];
    let { userId } = req.token;
    let listCharacters = req.body;

    creatingUser = await user.findOne({
        where: { id: userId }
    }).then(function (data) {
        return data;
    }).catch(error => {
        console.log(`Error looking for a user: ${error}`);
        return error;
    });

    if (creatingUser !== null && listCharacters != null) {
        for (const character of listCharacters) {


            if (character.deleteCharacterId != null) {

                let deletingList = character.deleteCharacterId;
                for (const element of deletingList) {

                    await cart.destroy({
                        where: {
                            characterId: element,
                            userId: userId
                        }
                    }).then(function (data) {
                        return data;
                    }).catch(error => {
                        console.log(`Error deleting a cart item: ${error}`);
                        return error;
                    });
                }




            } else {
                let item = await cart.findOne({
                    where: { characterId: character.characterId }
                }).then(function (data) {
                    return data;
                }).catch(error => {
                    console.log(`Error looking for a character in cart user: ${error}`);
                    return error;
                });

                if (item == null) {
                    await cart.create({
                        characterId: character.characterId,
                        image: character.characterImage,
                        characterName: character.characterName,
                        quantity: character.quantity,
                        userId: userId
                    }).then(function (data) {
                        return data;
                    }).catch(error => {
                        console.log(`Error adding item to cart: ${error}`);
                        return error;
                    });

                } else {
                    await cart.update({
                        quantity: character.quantity
                    }, {
                        where: {
                            characterId: character.characterId,
                            userId: userId
                        }
                    }).then(function (data) {
                        return data;
                    }).catch(error => {
                        console.log(`Error updating item: ${error}`);
                        return error;
                    });
                }
            }
        }
    } else {
        return res.status(200).json({ success: false, message: "Wrong data", data: [] });
    }
    return data;
}

async function getAllByUserId(req, res) {
    let data = [];
    let { userId } = req.token;

    if (userId == null) {
        return res.status(200).json({ success: false, message: `Can't get userId`, data: [] });
    }

    data = await cart.findAll({
        where: {
            userId: userId
        },
        attributes: [
            ['characterId', 'id'],
            'image',
            ['characterName', 'name'],
            'quantity'
        ]
    }).then(function (data) {
        return data;
    }).catch(error => {
        console.log(`Error getting cart from user: ${error}`);
        return error;
    });

    return data;
}

async function deleteByUserId() {
    let data = [];
    let { userId } = req.token;
    let { characterId } = req.body;

    if (userId == null) {
        return res.status(200).json({ success: false, message: `Can't get userId`, data: [] });
    }

    if (characterId == null || characterId == "") {
        return res.status(200).json({ success: false, message: `characterId can't be empty or null`, data: [] });
    }

    data = await cart.destroy({
        where: {
            characterId: characterId,
            userId: userId
        }
    }).then(function (data) {
        return data;
    }).catch(error => {
        console.log(`Error deleting a cart item: ${error}`);
        return error;
    });

    return data;
}



module.exports = { create, getAllByUserId, deleteByUserId }