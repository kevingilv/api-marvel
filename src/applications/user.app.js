let sequelize = require('../database/database');
let user = require('../models/user');
let jwt = require('jsonwebtoken');
let constants = require('../constants');

sequelize.sync();

async function create(req, res) {
    let data = [];
    let userExists = [];
    let { username, password, password2, name } = req.body;

    if (username == '' || username == null ||
        password == '' || password == null ||
        name == '' || name == null) {
        return res.status(200).json({ success: false, message: "Fields can't be empty or null", data: [] });
    }

    if (password !== password2) {
        return res.status(200).json({ success: false, message: "The pass are not the same", data: [] });
    }

    userExists = await user.findOne({
        where: { username: username }
    }).then(function (user) {
        return user;
    }).catch(error => {
        console.log(`Error comparing user: ${error}`);
        return error;
    });

    if (userExists !== null) {
        return res.status(200).json({ success: false, message: "User already exist", data: [] });
    }

    userId = await user.create({
        username: username,
        password: password,
        name: name
    }).then(function (data) {
        return data;
    }).catch(error => {
        console.log(`Error creating user: ${error}`);
        return error;
    });

    return data;
}

async function login(req, res) {
    let data = [];
    let { username, password } = req.body;

    data = await user.findOne({
        where: {
            username: username,
            password: password
        }
    }).then(function (data) {
        return data;
    }).catch(error => {
        console.log(`Error getting user: ${error}`);
        return error;
    });

    if (data == null) {
        return res.status(200).json({ success: false, message: "Incorrect user or pass", data: [] });
    } else {
        let payload = {
            name: data.name,
            userId: data.id
        };
        let token = jwt.sign(payload, constants.SECRET_KEY, {
            expiresIn: 31536
        });
        data = { token: token }
    }

    return data;
}


module.exports = { create, login }