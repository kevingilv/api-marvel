const express = require('express');
const router = express.Router();
const CartController = require('../controllers/cart.controller');

router.post('/add',CartController.create);
router.get('/getbyid',CartController.getAllByUserId);

module.exports = router;
